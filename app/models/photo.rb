class Photo < ActiveRecord::Base
    has_attached_file :image,
     styles: {
       thumb: "50x50#",
       small: "150x150>",
       medium: "250x250>"} 
  before_post_process :is_image?
  
  attr_accessible :description, :title, :image
  belongs_to :user
   
  validates :user_id, presence: true
  validates_attachment_presence :image
  validates_attachment_content_type :image, :content_type => /image/ 
  default_scope order: 'photos.created_at ASC'
  
   def is_image?
    !(image_content_type =~ /^image/).nil?
  end
end
