// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
//= require autocomplete-rails
//= require bootstrap
//= require_tree .

function monkeyPatchAutocomplete() {
     var oldFn = $.ui.autocomplete.prototype._renderItem;

      $.ui.autocomplete.prototype._renderItem = function( ul, item) {
          var re = new RegExp(this.term);
          var t = item.label.replace(re,"<span style='font-weight:bold;color:Blue;'>" + 
                  this.term + 
                  "</span>");
	  var gravatar = get_gravatar(item.email, 30)
	  var gravatar_code = '<img src="' + gravatar + '" />';
          return $( "<li></li>" )
              .data( "item.autocomplete", item )
              .append( "<a>" + gravatar_code + t + "</a>" )
              .appendTo( ul );
      };
}
jQuery(document).ready(function()  {
    monkeyPatchAutocomplete();
 $( ".autosuggest" ).autocomplete({
  source: "/autosuggest",
  minLength: 2,
  delay: 50
 });
 $('#search_field').bind('railsAutocomplete.select', function(event, data){
  window.location.href = "/users/" + data.item.id;
});
});
