class PhotosController < ApplicationController
  before_filter :correct_user, only: [:destroy]
  
  def create
    @user = current_user
    @photo = @user.photos.build(params[:photo])
    if @photo.save
      flash[:success] = "Photo created successfully!"
      redirect_to photos_user_path(@user)
    else
      render 'users/upload_photo'
    end
  end
  
def destroy
    @photo.destroy
    flash[:success] = "Photo deleted successfully!"
    redirect_to photos_user_path(current_user)
  end

    def correct_user
      @photo = current_user.photos.find_by_id(params[:id])
      if @photo.nil?
	flash[:error] = "You are not authorized to complete this action"
	redirect_to root_path 
      end
    end
end
