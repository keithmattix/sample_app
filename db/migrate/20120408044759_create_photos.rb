class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :photo_file_name
      t.string :description
      t.integer :photo_file_size
      t.string :title
      t.integer :user_id

      t.timestamps
    end
    add_index :photos, [:user_id, :created_at]
  end
end
