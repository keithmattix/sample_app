class RemoveFileNameAndSizeColumns < ActiveRecord::Migration
  def change
    remove_column :photos, :photo_file_name
    remove_column :photos, :photo_file_size
  end
end