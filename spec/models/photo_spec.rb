require 'spec_helper'

describe Photo do
  let(:user) { FactoryGirl.create(:user) }
  before { @photo = user.photos.build(description: "Lorem ipsum", title: "Lorem_Ipsum") }

  subject { @photo }

  it { should respond_to(:description) }
  it { should respond_to(:user_id) }
  it { should respond_to(:title) }
  it { should respond_to(:user) } 
  its(:user) { should == user }
  
  describe "when user_id is not present" do
    before { @photo.user_id = nil }
    it {should_not be_valid }
  end
  
  describe "accessible attributes" do
    it "should not allow access to user_id" do
      expect do
        Photo.new(user_id: user.id)
      end.should raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end    
  end
end
